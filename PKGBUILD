# Maintainer: Giancarlo Razzolini <grazzolini@archlinux.org>
pkgname=mkinitcpio-utils
pkgver=0.0.4
pkgrel=3
pkgdesc="Collection of Archlinux mkinitcpio utilities performing various tasks"
arch=('any')
url="https://github.com/grazzolini/mkinitcpio-utils"
license=('BSD')
depends=('cryptsetup')
optdepends=('mkinitcpio-dropbear: Allow the encryptssh hook to unlock a root container remotely using dropbear'
            'mkinitcpio-tinyssh: Allow the encryptssh hook to unlock a root container remotely using tinyssh')
changelog='ChangeLog'
source=("${pkgname}-${pkgver}.tar.gz::$url/archive/v$pkgver.tar.gz"
        "extra-arguments.patch"
        "0001-add-duress.patch")
sha512sums=('15e9cae788df4727dc148090831e0bfaeefd0ecae49ac9f0cbab4ded4639841d151cd291658055d9385a8f49bb91aee1d6ab8d5f6b0cc161586a9e355457cad0'
            '1539955032acfb805e9ba2d130775725f0bb5ac13a171520826fff338dd1909ef201733d4249fd31edf7e570eb546f55eeff04953ae30416ac77d1237b0be72e'
            '81bb1eb3e2b2b25b9f64d34fdeb5286733f0f151067b5772b6194bf760c40bd30bf1f6b6b9f6ec7f4ab912953ec1985f3a4921a86576ac5e3791c34893e26e41')
backup=(etc/encryptssh-duress)

prepare() {
  cd "$srcdir/$pkgname-$pkgver"
  patch --forward --strip=1 --input=../extra-arguments.patch
  patch --forward --strip=1 --input=../0001-add-duress.patch
}

package() {
  install -Dm755 "$srcdir/$pkgname-$pkgver/utils/shells/cryptsetup_shell"       "$pkgdir/usr/share/$pkgname/utils/shells/cryptsetup_shell"
  install -Dm755 "$srcdir/$pkgname-$pkgver/utils/unlock_password"               "$pkgdir/usr/share/$pkgname/utils/unlock_password"
  install -Dm644 "$srcdir/$pkgname-$pkgver/README.md"                           "$pkgdir/usr/share/$pkgname/README.md"
  install -Dm600 "$srcdir/$pkgname-$pkgver/encryptssh-duress"                   "$pkgdir/etc/encryptssh-duress"
  install -Dm644 "$srcdir/$pkgname-$pkgver/initcpio/hooks/encryptssh"           "$pkgdir/usr/lib/initcpio/hooks/encryptssh"
  install -Dm644 "$srcdir/$pkgname-$pkgver/initcpio/install/encryptssh"         "$pkgdir/usr/lib/initcpio/install/encryptssh"
  install -Dm644 "$srcdir/$pkgname-$pkgver/LICENSE"                             "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
